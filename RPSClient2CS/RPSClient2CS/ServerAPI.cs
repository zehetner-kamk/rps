/*
 * Class that will take care of all communication with server.
 * Author: Clemens Zehetner
 */

using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace RockPaperScissors {
    public class ServerAPI {
        private UdpClient sender;
        private IPEndPoint serverPoint;
        private UdpClient receiver;
        private IPEndPoint clientPoint;

        public enum MsgT { t, r, v, p, m, s, q, e } //test, register, validate, pair, multiplayer, singleplayer, queue, error, null
        public enum InfoT { Connection, Registration, Validation, PairReqd, PairActd, PairDeactd, PairTimeouted, MultiStarted, MultiResult, SingleSent, SingleResult, Query, GenericError }
        public delegate void InfoCallbackT(InfoT newInfo, bool isError, string[] content);
        private InfoCallbackT InfoCallback;


        public ServerAPI(string ip, int port, InfoCallbackT infoCallback) {
            int clientPort = new Random().Next(20000, 30000); //local port gets randomly assigned
            this.sender = new UdpClient(clientPort);
            this.serverPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            this.receiver = new UdpClient(clientPort + 1); //receiving port is 1 higher than sending port
            this.clientPoint = new IPEndPoint(IPAddress.Any, clientPort + 1);
            this.InfoCallback = infoCallback;

            new Thread(() => { //receives packages and calls callback method
                this.UDPReceive();
            }).Start();
        }

        public void TestConnection() {
            string message = "";
            UDPSend(MsgT.t, message);
        }

        public void Register(ref Player player) {
            string message = "-1," + player.password;
            UDPSend(MsgT.r, message);
        }

        public void ValidateCredentials(ref Player player) {
            string message = player.id + "," + player.password;
            UDPSend(MsgT.v, message);
        }

        public void PairWithPlayer(ref Player player, int friendID, int numOfElements = 3) {
            string message = player.id + "," + player.password + "," + friendID + "," + numOfElements;
            UDPSend(MsgT.p, message);
        }

        public void MultiplayerGame(ref Player player) {
            string message = player.id + "," + player.password + "," + player.Choice;
            UDPSend(MsgT.m, message);
        }


        public void SingleplayerGame(ref Player player) {
            string message = player.id + "," + player.password + "," + player.Choice;
            UDPSend(MsgT.s, message);
        }


        private void UDPSend(MsgT type, string message) {
            string m = type.ToString() + "," + message;
            sender.Send(Encoding.UTF8.GetBytes(m), m.Length, serverPoint);
        }
        private void UDPReceive() { //receives UDP messages from the server and forwards them to Program.InfoCallback()
            while (true) {
                string[] message = new string[1];
                try {
                    message = Encoding.UTF8.GetString(receiver.Receive(ref clientPoint)).Replace("\0", "").Split(',');
                } catch (SocketException) {
                    return; //ShutDown() closes receiver -> program should end
                }

                MsgT messageType = (MsgT)Enum.Parse(typeof(MsgT), message[0]);
                message = message.Skip(1).ToArray();

                switch (messageType) {
                    case MsgT.t: //TEST
                        InfoCallback(InfoT.Connection, false, message);
                        break;
                    case MsgT.r: //REGISTRATION
                        InfoCallback(InfoT.Registration, false, message);
                        break;
                    case MsgT.v:
                        InfoCallback(InfoT.Validation, false, message);
                        break;
                    case MsgT.p: //PAIRING
                        if (message.Length == 4) {
                            string t = message[0];
                            message = message.Skip(1).ToArray();

                            switch (t) {
                                case "r": //requested
                                    InfoCallback(InfoT.PairReqd, false, message);
                                    break;
                                case "a": //activated
                                    InfoCallback(InfoT.PairActd, false, message);
                                    break;
                                case "d": //deactivated
                                    InfoCallback(InfoT.PairDeactd, false, message);
                                    break;
                                case "t": //timeouted
                                    InfoCallback(InfoT.PairTimeouted, false, message);
                                    break;
                            }
                        } else {
                            string[] errorMsg = new string[1];
                            errorMsg[0] = "Unknown server error while handling player connection.";
                            InfoCallback(InfoT.GenericError, true, errorMsg);
                        }
                        break;

                    case MsgT.m: //MULTIPLAYER GAME
                        if (message.Length == 2) {
                            string t = message[0];
                            message = message.Skip(1).ToArray();

                            switch (t) {
                                case "s":  //started
                                    InfoCallback(InfoT.MultiStarted, false, message);
                                    break;
                                case "r": //results
                                    InfoCallback(InfoT.MultiResult, false, message);
                                    break;
                            }
                        } else {
                            string[] errorMsg = new string[1];
                            errorMsg[0] = "Unknown server error while handling multiplayer game.";
                            InfoCallback(InfoT.GenericError, true, errorMsg);
                        }
                        break;

                    case MsgT.s: //SINGLEPLAYER GAME
                        InfoCallback(InfoT.SingleResult, false, message);
                        break;

                    case MsgT.q:
                        InfoCallback(InfoT.Query, false, message);
                        break;

                    case MsgT.e: //ERROR MESSAGES
                        if (message.Length == 2) {
                            MsgT originType;
                            try {
                                originType = (MsgT)Enum.Parse(typeof(MsgT), message[0]); //origin of error message
                            } catch (ArgumentException) {
                                originType = MsgT.e;
                            }
                            message = message.Skip(1).ToArray();

                            switch (originType) {
                                case MsgT.r:
                                    InfoCallback(InfoT.Registration, true, message);
                                    break;
                                case MsgT.v:
                                    InfoCallback(InfoT.Validation, true, message);
                                    break;
                                case MsgT.p:
                                    InfoCallback(InfoT.PairReqd, true, message);
                                    break;
                                case MsgT.m:
                                    InfoCallback(InfoT.MultiStarted, true, message);
                                    break;
                                case MsgT.s:
                                    InfoCallback(InfoT.SingleSent, true, message);
                                    break;
                                case MsgT.q:
                                    InfoCallback(InfoT.Query, true, message);
                                    break;
                                default: //error message that is not generated by specific server handler
                                    InfoCallback(InfoT.GenericError, true, message);
                                    break;
                            }
                        }
                        break;
                }

            }
            Console.WriteLine("ended");
        }

        public void ShutDown() {
            receiver.Close();
        }
    }
}