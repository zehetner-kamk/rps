﻿/*
 * Title: Rock, Paper, Scissors
 * Author: Joona Tolonen
 */
using System;
using System.Threading;
using static System.Console;
using static System.Int32;

namespace RockPaperScissors {
    public struct Player {
        public int id;
        public string password;
        public string Name;
        public bool IsHuman;
        public int Choice;
        public string[] ChoiceName;
    }

    internal static class Program {
        private static ServerAPI server=new ServerAPI("86.50.253.157", 30010, InfoCallback); //cPouta IP

        private static Player p1;
        private static Player p2;

        private static bool connected = false;
        private static int validated = -1; //-1 not valid, 0 pending, 1 valid
        private static bool waitForResult;

        private static void Main() {
            WriteLine("Welcome to play Rock, Paper, Scissors!");

            server.ShutDown();

            InitServer();

            while (validated < 1) { //initialise and validate player
                if (validated == -1) {
                    p1 = InitPlayer(true);
                    if (p1.id == -1) RegisterPlayer();
                    server.ValidateCredentials(ref p1);
                    ++validated;
                }

                Thread.Sleep(100);
            }
            p2 = InitPlayer(false); //computer initialisation
            

            Console.Write("How many rounds will be played? ");
            if (!TryParse(Console.ReadLine(), out var rounds)) {
                Console.WriteLine("Invalid input, will use 5.");
                rounds = 5;
            }

            Console.WriteLine("Excellent! Let's play the best out of {0} rounds.", rounds);

            for (var i = 0; i < rounds; i++) {
                MakeChoice(true);
                MakeChoice(false);
                waitForResult = true;

                while (waitForResult) {
                    Thread.Sleep(100);
                }
            }

            server.ShutDown();
        }

        private static void InitServer() {
            while (!connected) {
                Console.Write("Server IP:   ");
                string ip = ReadLine().Replace("\n", "");
                bool validPort = false;
                while (!validPort) {
                    Console.Write("Server Port: ");
                    if (TryParse(ReadLine(), out int port)) {
                        server = new ServerAPI(ip, port, InfoCallback);
                        validPort = true;
                    }
                }

                server.TestConnection();
                for (int i = 0; i < 50; ++i) {
                    if (!connected) Thread.Sleep(100);
                }
                if (!connected) Console.WriteLine("Error: Could not connect to server.");
            }
        }

        private static Player InitPlayer(bool isHuman) {
            Player player = new Player {
                IsHuman = isHuman,
                Choice = 0,
                ChoiceName = new[] { "Rock", "Paper", "Scissors" }
            };

            if (isHuman) {
                string input = "";
                /*while (input != null && input.Length == 0) {
                    Write("What is your name? ");
                    input = ReadLine();
                }
                player.Name = input.Replace("\n", "");/*/
                player.Name = "You";
                input = "";

                while (input != null && input.Length == 0) {
                    Console.WriteLine("Please enter your ID (-1 if you don't have one): ");
                    input = Console.ReadLine();
                }
                if (TryParse(input, out var id)) {
                    player.id = id;
                } else {
                    player.id = 0;
                }
                input = "";

                while (input != null && input.Length == 0) {
                    Console.WriteLine("Please enter your password (make one up if you don't have one): ");
                    input = Console.ReadLine();
                }
                player.password = input.Replace("\n", "");


            } else {
                player.Name = "Computer";
            }

            return player;
        }

        private static void MakeChoice(bool isHuman) {
            if (!isHuman) {
                server.SingleplayerGame(ref p1);
            } else {
                while (true) {
                    Write("Select (R)ock, (P)aper, or (S)cissors: ");

                    var userInput = ReadLine()?.Trim().ToLower();

                    if (userInput != null && userInput.StartsWith("r")) {
                        p1.Choice = 0;
                        break;
                    }

                    if (userInput != null && userInput.StartsWith("p")) {
                        p1.Choice = 1;
                        break;
                    }

                    if (userInput != null && userInput.StartsWith("s")) {
                        p1.Choice = 2;
                        break;
                    }
                }
            }
        }

        private static void WhoWon() {
            if (p1.Choice == p2.Choice) {
                Console.WriteLine("Draw!");
            } else if ((p1.Choice == 0 && p2.Choice == 2) ||
                       (p1.Choice == 1 && p2.Choice == 0) ||
                       (p1.Choice == 2 && p2.Choice == 1)) {
                Console.WriteLine($"{p1.Name} won!");
            } else {
                Console.WriteLine($"{p2.Name} won!");
            }
        }

        private static void RegisterPlayer() {
            if (p1.id < 0) {
                bool pending = false;
                while (p1.id < 0) {
                    //registration of new player
                    if (!pending) {
                        server.Register(ref p1);
                        pending = true;
                    }
                    Thread.Sleep(100);

                    if (p1.id == -2) {
                        //something went wrong during registration
                        Console.WriteLine("Please try again.");
                        p1 = InitPlayer(true);
                        pending = false;
                    }
                }
                Console.WriteLine("Successfully registered. Your ID is " + p1.id + ". Please remember your password.");
            }
        }

        private static void InfoCallback(ServerAPI.InfoT newInfo, bool isError, string[] content) { //runs in a separate thread
        //gets called by ServerAPI when it receives a message from the server and handles it
            if (isError) { //error handling
                switch (newInfo) {
                    case ServerAPI.InfoT.Registration:
                        Console.WriteLine("Error while registering player: " + content[0]);
                        p1.id = -2;
                        break;
                    case ServerAPI.InfoT.Validation:
                        Console.WriteLine("Error: " + content[0]);
                        validated = -1;
                        break;
                    case ServerAPI.InfoT.PairReqd:
                        Console.WriteLine("Error while requesting pairing: " + content[0]);
                        break;
                    case ServerAPI.InfoT.MultiStarted:
                        Console.WriteLine("Error while playing multiplayer game: " + content[0]);
                        break;
                    case ServerAPI.InfoT.SingleResult:
                        Console.WriteLine("Error while playing singleplayer game: " + content[0] + " Match will end in a draw.");
                        p2.Choice = p1.Choice;
                        waitForResult = false;
                        break;
                    case ServerAPI.InfoT.Query:
                        Console.WriteLine("Error while querying past games: " + content[0]);
                        break;
                    default:
                        Console.WriteLine("Error: " + content[0]);
                        break;
                }
                return;
            }

            string text;
            switch (newInfo) { //confirmation/information message handling
                case ServerAPI.InfoT.Connection:
                    Console.WriteLine("Connection to server established.");
                    connected = true;
                    break;
                case ServerAPI.InfoT.Registration:
                    int playerID = int.Parse(content[0]);
                    p1.id = playerID;
                    break;
                case ServerAPI.InfoT.Validation:
                    Console.WriteLine("Credential validation successful.");
                    validated = 1;
                    break;
                case ServerAPI.InfoT.PairReqd:
                    text = "Pairing to player " + content[1] + " successfully requested.";
                    if (int.Parse(content[2]) != 3) text += " Number of elements: " + content[2] + ".";
                    Console.WriteLine(text);
                    break;
                case ServerAPI.InfoT.PairActd:
                    Console.WriteLine("Pair " + content[0] + "-" + content[1] + " activated.");
                    break;
                case ServerAPI.InfoT.PairDeactd:
                    Console.WriteLine("Pair " + content[0] + "-" + content[1] + " deactivated.");
                    break;
                case ServerAPI.InfoT.PairTimeouted:
                    Console.WriteLine("Pair " + content[0] + "-" + content[1] + " timeouted.");
                    break;
                case ServerAPI.InfoT.MultiStarted:
                    Console.WriteLine("Player " + content[0] + " has started a game.");
                    break;
                case ServerAPI.InfoT.MultiResult:
                    int friendPick = int.Parse(content[0]);
                    //do something with the result
                    break;
                case ServerAPI.InfoT.SingleResult:
                    int serverPick = int.Parse(content[0]);
                    WriteLine("Server selected: " + serverPick);
                    p2.Choice = serverPick;
                    WhoWon();
                    waitForResult = false;
                    break;
                case ServerAPI.InfoT.Query:
                    text = "Game information: " + new DateTime(long.Parse(content[2])) + ", player " + content[0] + " picked " + content[3] + ", player " + content[1] + " picked " + content[4];
                    if (int.Parse(content[5]) != 3) text += ", number of elements " + content[5];
                    Console.WriteLine(text);
                    break;
            }
        }
    }
}