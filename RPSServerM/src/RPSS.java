import logic.impl.MessageHandlerImpl;
import network.UDPReceiver;
import network.documentationServer.DocumentationServer;

public class RPSS {
    public static void main(String[] args) {
        System.out.println("starting...");

        new Thread(()->{
            MessageHandlerImpl handler = new MessageHandlerImpl();
            UDPReceiver.start();
        }).start();


        //Since RPSS runs on the same cPouta instance as JoystickServer, only one documentation server is needed, which in this case is provided by JServer's nodeJS.
        //If you want to run this standalone, you can enable this line to activate the documentation server on port 8080.
        //DocumentationServer.start(); //HTTPServer must not run in side thread
    }
}
