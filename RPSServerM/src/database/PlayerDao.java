package database;

import database.domainObjects.PlayerDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PlayerDao {
    private static final String TABLE_NAME = "player";

    public int insert(PlayerDO player) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (password) VALUES (?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, player.getPassword());

                statement.executeUpdate();
                ResultSet result = statement.getGeneratedKeys();
                if (result.next()) {
                    return result.getInt(1);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public PlayerDO getByID(int id) {
        String selectStatement = "SELECT * FROM " + TABLE_NAME + " WHERE id=(?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(selectStatement)) {
                statement.setInt(1, id);

                ResultSet result = statement.executeQuery();
                if (result.next()) {
                    return new PlayerDO(result.getInt(1), result.getString(2));
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<Integer> getIDs() {
        String selectStatement = "SELECT id FROM " + TABLE_NAME;

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(selectStatement)) {

                ResultSet result = statement.executeQuery();
                List<Integer> ids = new ArrayList<>();
                while (result.next()) {
                    ids.add(result.getInt(1));
                }
                return ids;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }
}
