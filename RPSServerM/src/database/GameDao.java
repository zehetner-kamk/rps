package database;

import database.domainObjects.GameDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GameDao {
    private static final String TABLE_NAME = "game";

    public boolean insert(GameDO game) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (client1, client2, time, c1pick, c2pick, numofelements) VALUES (?, ?, ?, ?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, game.getID1());
                statement.setInt(2, game.getID2());
                statement.setTimestamp(3, new Timestamp(game.getTime()));
                statement.setInt(4, game.getC1pick());
                statement.setInt(5, game.getC2pick());
                statement.setInt(6, game.getNumOfElements());

                if (statement.execute()) {
                    System.out.println("saved to db");
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<GameDO> getGamesByPlayerID(int id) {
        String selectStatement = "SELECT * FROM " + TABLE_NAME + " WHERE client1=(?) OR client2=(?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(selectStatement)) {
                statement.setInt(1, id);
                statement.setInt(2, id);

                ResultSet result = statement.executeQuery();
                List<GameDO> games = new ArrayList<>();
                while (result.next()) {
                    GameDO game = new GameDO(result.getInt(2), result.getInt(3), result.getTimestamp(4).getTime(), result.getInt(5), result.getInt(6), result.getInt(7));
                    games.add(game);
                }

                return games;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }
}
