package database.domainObjects;

public class GameDO {
    private Integer id1;
    private Integer id2;
    private long time;
    private Integer c1pick;
    private Integer c2pick;
    private Integer numOfElements;

    public GameDO(Integer id1, Integer id2, long time, Integer c1pick, Integer c2pick, Integer numOfElements) {
        this.id1 = id1;
        this.id2 = id2;
        this.time = time;
        this.c1pick = c1pick;
        this.c2pick = c2pick;
        this.numOfElements = numOfElements;
    }

    public Integer getID1() {
        return id1;
    }

    public void setID1(Integer id1) {
        this.id1 = id1;
    }

    public Integer getID2() {
        return id2;
    }

    public void setID2(Integer id2) {
        this.id2 = id2;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getC1pick() {
        return c1pick;
    }

    public void setC1pick(int c1pick) {
        this.c1pick = c1pick;
    }

    public Integer getC2pick() {
        return c2pick;
    }

    public void setC2pick(Integer c2pick) {
        this.c2pick = c2pick;
    }

    public Integer getNumOfElements() {
        return numOfElements;
    }

    public void setNumOfElements(Integer numOfElements) {
        this.numOfElements = numOfElements;
    }

    @Override
    public String toString() {
        return id1 + "," + id2 + "," + time + "," + c1pick + "," + c2pick + "," + numOfElements;
    }
}
