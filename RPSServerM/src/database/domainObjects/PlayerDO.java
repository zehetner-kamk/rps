package database.domainObjects;

public class PlayerDO {
    private int id; //same as client ID
    private String password;

    public PlayerDO(int id, String password) {
        this.id = id;
        this.password = password;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
