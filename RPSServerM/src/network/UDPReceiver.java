package network;

import logic.MessageHandler;
import logic.impl.MessageHandlerImpl;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPReceiver {
    private static boolean stop;
    private static final int RECEIVE_PORT = 30010;
    private static MessageHandler handler;

    public static void start() { //endless while loop that receives UDP packets and notifies messageHandlers
        DatagramSocket socket = null;
        stop = false;
        while (!stop) {
            try {
                while (socket == null || socket.isClosed()) {
                    socket = new DatagramSocket(RECEIVE_PORT);
                }

                DatagramPacket packet = new DatagramPacket(new byte[64], 64);
                socket.receive(packet);
                System.out.println("Received " + new String(packet.getData(), "UTF-8"));

                String content = new String(packet.getData(), "UTF-8");

                Message.MsgT type;
                try {
                    type = Message.MsgT.valueOf(content.substring(0, 1));
                } catch (IllegalArgumentException e) {
                    type = Message.MsgT.e; //error message
                }
                Message m = new Message(type, content.substring(2).replace("\0",""), packet.getAddress().toString().substring(1), packet.getPort(), null, RECEIVE_PORT);
                handler.handleMessage(m);

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }

            }
        }
    }

    public static void stop() {
        stop = true;
    }

    public static void setMessageHandler(MessageHandler h) {
        handler = h;
    }
}