package network.documentationServer;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import logic.MessageHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class HttpHandlerImpl implements com.sun.net.httpserver.HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException { //gets called when http request is made to server

        StringBuilder sb = new StringBuilder();
        Files.lines(Paths.get("/home/ubuntu/rps/RPSServerM/documentation.html")).forEach(sb::append);
        String html = sb.toString();

        httpExchange.sendResponseHeaders(200, html.length());
        httpExchange.getResponseBody().write(html.getBytes());
        httpExchange.close();
    }
}