package network.documentationServer;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import sun.net.httpserver.HttpServerImpl;

import java.net.InetSocketAddress;

public class DocumentationServer {
    private static final int SERVER_PORT = 8080;


    public static void start() {
        HttpServer httpServer = null;
        while (httpServer == null) {
            try {
                httpServer = HttpServerImpl.create(new InetSocketAddress(SERVER_PORT), 0);
                HttpContext context = httpServer.createContext("/");
                context.setHandler(new HttpHandlerImpl());

                httpServer.start();
            } catch (Exception e) {
                e.printStackTrace();

                if (httpServer != null) { //stops and resets httpServer, so it starts itself again after 10 sec
                    httpServer.stop(0);
                    httpServer = null;
                }
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }
}
