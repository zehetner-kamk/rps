package logic;

import network.Message;

public interface MessageHandler {
    void handleMessage(Message m);
}
