package logic;

import database.GameDao;
import database.PlayerDao;
import logic.datastructures.Client;
import logic.datastructures.ClientPair;
import logic.datastructures.ServerState;
import network.Message;
import network.UDPSender;

import java.util.List;

public abstract class MessageHandlerAbstract implements MessageHandler {
    protected PlayerDao playerDao = new PlayerDao();
    protected GameDao gameDao = new GameDao();

    public abstract void handleMessage(Message m);

    protected int parseInteger(String s, Message.MsgT type, String ip, int port) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Integer " + s + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }

    protected boolean minContentLength(String[] content, int minLength, Message.MsgT type, String ip, int port) {
        boolean valid = content.length >= minLength;
        if (!valid) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Invalid message content.", null, null, ip, port));
        }
        return valid;
    }

    protected boolean validateID(int id, Message.MsgT type, boolean send, String ip, int port) {
        List<Integer> ids = playerDao.getIDs();
        ids.remove(Integer.valueOf(-1)); //-1 is reserved as server id for singleplayer games
        boolean valid = ids.contains(id);
        if (!valid && send) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Invalid ID.", null, null, ip, port));
        }
        return valid;
    }

    protected boolean validateCredentials(Client client, Message.MsgT type) {
        boolean valid = validateID(client.getPlayer().getID(), type, false, client.getIP(), client.getPort()); //id

        if (valid && !playerDao.getByID(client.getPlayer().getID()).getPassword().equals(client.getPlayer().getPassword())) { //password
            valid = false;
        }

        if (!valid) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Invalid login credentials.", null, null, client.getIP(), client.getPort()));
        }
        return valid; //successfully logged in
    }

    protected ClientPair getClientPair(int id) {
        for (ClientPair p : ServerState.getClientPairs()) {
            if (p.isActive() && p.getC1() != null && p.getC1().getPlayer().getID() == id || p.getC2() != null && p.getC2().getPlayer().getID() == id) {
                return p;
            }
        }
        return null;
    }

    protected boolean validatePick(int pick, int numOfElements, Message.MsgT type, String ip, int port) {
        boolean valid = pick >= 0 && pick < numOfElements;
        if (!valid) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Invalid pick.", null, null, ip, port));
        }
        return valid;
    }

    protected boolean validateNumOfElements(int numOfElements, Message.MsgT type, String ip, int port) {
        boolean valid = numOfElements >= 3; //rps has to have at least 3 elements
        if (!valid) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Invalid number of elements.", null, null, ip, port));
        }
        return valid;
    }


    protected void printClientPairs() {  //for debug purposes only
        for (ClientPair p : ServerState.getClientPairs()) {
            if (p != null) {
                System.out.println(p.toString());
            } else {
                System.out.println("null");
            }
        }
    }
}
