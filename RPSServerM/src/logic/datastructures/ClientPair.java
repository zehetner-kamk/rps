package logic.datastructures;

public class ClientPair {
    private Client c1;
    private Client c2;
    private long time;
    private int numOfElements;
    private boolean active;

    public ClientPair(Client c1, Client c2, int numOfElements) {
        this.c1 = c1;
        this.c2 = c2;
        this.numOfElements = numOfElements;
        this.time = System.currentTimeMillis();
        this.active = false;
    }

    public Client getC1() {
        return c1;
    }

    public void setC1(Client id1) {
        this.c1 = id1;
    }

    public Client getC2() {
        return c2;
    }

    public void setC2(Client id2) {
        this.c2 = id2;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getNumOfElements() {
        return numOfElements;
    }

    public void setNumOfElements(int numOfElements) {
        this.numOfElements = numOfElements;
    }

    @Override
    public String toString() {
        return c1 + "," + c2 + "," + time + "," + numOfElements + "," + active;
    }
}
