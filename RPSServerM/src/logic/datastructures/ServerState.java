package logic.datastructures;

import database.domainObjects.GameDO;
import network.Message;
import network.UDPSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ServerState {
    private static final int CONNECTION_TIMEOUT_MINS = 10;
    private static List<ClientPair> clientPairs = Collections.synchronizedList(new ArrayList<>());
    private static List<GameDO> games = Collections.synchronizedList(new ArrayList<>());

    public static List<ClientPair> getClientPairs() {
        return clientPairs;
    }

    public static List<GameDO> getGames() {
        return games;
    }

    static { //expired pairs get timeouted and removed
        new Thread(() -> {
            while (true) {
                for (ClientPair p : clientPairs) {
                    if (System.currentTimeMillis() - p.getTime() > TimeUnit.MINUTES.toMillis(CONNECTION_TIMEOUT_MINS)) {
                        String text = "t," + p.getC1().getPlayer().getID() + "," + p.getC2().getPlayer().getID() + "," + p.getNumOfElements(); //timeouted
                        UDPSender.send(new Message(Message.MsgT.p, text, null, null, p.getC1().getIP(), p.getC1().getPort()));
                        UDPSender.send(new Message(Message.MsgT.p, text, null, null, p.getC2().getIP(), p.getC2().getPort()));
                    }
                }
                ServerState.getClientPairs().removeIf(p -> (new Date(System.currentTimeMillis()).getTime()) - p.getTime() > TimeUnit.MINUTES.toMillis(CONNECTION_TIMEOUT_MINS));

                try {
                    Thread.sleep(TimeUnit.MINUTES.toMillis(CONNECTION_TIMEOUT_MINS));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
