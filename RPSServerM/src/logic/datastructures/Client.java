package logic.datastructures;

import database.domainObjects.PlayerDO;

public class Client {
    private PlayerDO player;
    private String ip;
    private int port;

    public Client(PlayerDO player, String ip, int port) {
        this.player = player;
        this.ip = ip;
        this.port = port;
    }

    public PlayerDO getPlayer() {
        return player;
    }

    public void setPlayer(PlayerDO player) {
        this.player = player;
    }

    public String getIP() {
        return ip;
    }

    public void setIP(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
