package logic.impl;

import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

public class RegistrationHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    @Override
    public void handleMessage(Message m) { //"-1,pwd"
        String[] content = m.getContent().split(",");
        if (content.length != 2) {
            UDPSender.send(new Message(Message.MsgT.e, "r,Password must not contain ','.", null, null, m.getSourceIP(), m.getSourcePort()));
            return;
        }
        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.r, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());

        if (player.getID() == -2) return;

        //register new player
        if (player.getID() == -1) {
            if (player.getPassword().length() > 32) {
                UDPSender.send(new Message(Message.MsgT.e, "r,Maximum password length is 32.", null, null, client.getIP(), client.getPort()));
                return;
            }

            player.setID(playerDao.insert(player));
            UDPSender.send(new Message(Message.MsgT.r, player.getID() + "", null, null, client.getIP(), client.getPort()));
        }
    }
}
