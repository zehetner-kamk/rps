package logic.impl;

import database.domainObjects.GameDO;
import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

import java.util.List;

public class QueryHandlerImpl extends MessageHandlerAbstract { //receives query messages, provides player data about finished games

    @Override
    public void handleMessage(Message m) {
        String[] content = m.getContent().split(",");
        if (!minContentLength(content, 2, Message.MsgT.q, m.getSourceIP(), m.getSourcePort())) return;

        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.q, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());

        if (player.getID() == -2 || !validateCredentials(client, Message.MsgT.q)) return;

        List<GameDO> games = gameDao.getGamesByPlayerID(client.getPlayer().getID());
        for (GameDO g : games) { //sends out all games in which player has participated
            UDPSender.send(new Message(Message.MsgT.q, g.toString(), null, null, client.getIP(), client.getPort()));
        }
    }
}
