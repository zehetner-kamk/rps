package logic.impl;

import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

public class ValidationHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    @Override
    public void handleMessage(Message m) {
        String[] content = m.getContent().split(",");
        if (!minContentLength(content, 2, Message.MsgT.m, m.getSourceIP(), m.getSourcePort())) return;
        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.m, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());

        if (validateCredentials(client, Message.MsgT.v))
            UDPSender.send(new Message(Message.MsgT.v, "", null, null, client.getIP(), client.getPort()));
    }
}
