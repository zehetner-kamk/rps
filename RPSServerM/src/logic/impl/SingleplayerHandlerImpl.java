package logic.impl;

import database.domainObjects.GameDO;
import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

import java.util.Random;


public class SingleplayerHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    Random rand = new Random();

    @Override
    public void handleMessage(Message m) { //"id,pwd,pick[,numOfElements]"
        String[] content = m.getContent().split(",");
        if (!minContentLength(content, 3, Message.MsgT.s, m.getSourceIP(), m.getSourcePort())) return;
        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.s, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());
        int pick = parseInteger(content[2], Message.MsgT.s, m.getSourceIP(), m.getSourcePort());

        int numOfElements = 3;
        if (content.length == 4) { //player sets custom game size
            numOfElements = parseInteger(content[3], Message.MsgT.s, m.getSourceIP(), m.getSourcePort());
        }
        if (player.getID() == -2 || !validateCredentials(client, Message.MsgT.s) || !validatePick(pick, numOfElements, Message.MsgT.s, m.getSourceIP(), m.getSourcePort()) || !validateNumOfElements(numOfElements, Message.MsgT.s, m.getSourceIP(), m.getSourcePort()))
            return;


        GameDO game = new GameDO(player.getID(), -1, System.currentTimeMillis(), pick, rand.nextInt(numOfElements), numOfElements);
        gameDao.insert(game);
        UDPSender.send(new Message(Message.MsgT.s, game.getC2pick() + "", null, null, client.getIP(), client.getPort()));
    }
}
