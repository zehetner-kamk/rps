package logic.impl;

import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.datastructures.ClientPair;
import logic.datastructures.ServerState;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

public class PairingHandlerImpl extends MessageHandlerAbstract { //receives connection messages, creates and activates player pairs
    private static final int CONNECTION_TIMEOUT_MINS = 10;


    @Override
    public void handleMessage(Message m) { //"id,pwd,friend-id[,numOfElements]"
        String[] content = m.getContent().split(",");
        if (!minContentLength(content, 3, Message.MsgT.p, m.getSourceIP(), m.getSourcePort())) return;
        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.p, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());
        int id2 = parseInteger(content[2], Message.MsgT.p, m.getSourceIP(), m.getSourcePort());

        int numOfElements = 3;
        if (content.length == 4) { //player sets custom number of elements
            numOfElements = parseInteger(content[3], Message.MsgT.p, m.getSourceIP(), m.getSourcePort());
        }

        //check if both IDs are correct and log client in
        if (player.getID() == -2 || !validateCredentials(client, Message.MsgT.p) || id2 == -2 || !validateID(id2, Message.MsgT.p, true, client.getIP(), client.getPort()) || !validateNumOfElements(numOfElements, Message.MsgT.p, client.getIP(), client.getPort()))
            return;


        ClientPair pair = getClientPair(player.getID()); //retrieve client pair if exists
        if (pair == null) pair = getClientPair(id2);

        if (pair == null) {
            createPair(client, id2, numOfElements);
            String text = "r," + player.getID() + "," + id2 + "," + numOfElements; //requested
            UDPSender.send(new Message(Message.MsgT.p, text, null, null, client.getIP(), client.getPort()));

        } else {
            if (pair.getC1().getPlayer().getID() == id2 && pair.getC2().getPlayer().getID() == player.getID()) {
                if (pair.getNumOfElements() == numOfElements) {
                    //Client A created pair (A,B) some time ago. Now player B sent message containing (B,A). This confirms that both players want to connect, therefore pair is set to active.
                    //This only happens if first request was not older than 10 minutes

                    pair.setC2(client); //dummy client created during A's request gets replaced with real B client
                    pair.setTime(System.currentTimeMillis());

                    for (ClientPair p : ServerState.getClientPairs()) { //deactivate every other pair in which A or B are part of
                        if (p.isActive() &&
                                (p.getC1().getPlayer().getID() == pair.getC1().getPlayer().getID() || p.getC1().getPlayer().getID() == pair.getC2().getPlayer().getID() ||
                                        p.getC2().getPlayer().getID() == pair.getC1().getPlayer().getID() || p.getC2().getPlayer().getID() == pair.getC2().getPlayer().getID())) {
                            String text = "d," + p.getC1().getPlayer().getID() + "," + p.getC2().getPlayer().getID() + "," + p.getNumOfElements(); //deactivated
                            UDPSender.send(new Message(Message.MsgT.p, text, null, null, p.getC1().getIP(), p.getC1().getPort()));
                            UDPSender.send(new Message(Message.MsgT.p, text, null, null, p.getC2().getIP(), p.getC2().getPort()));
                            p.setActive(false);
                        }

                    }
                    pair.setActive(true);

                    String text = "a," + pair.getC1().getPlayer().getID() + "," + pair.getC2().getPlayer().getID() + "," + pair.getNumOfElements(); //activated
                    UDPSender.send(new Message(Message.MsgT.p, text, null, null, pair.getC1().getIP(), pair.getC1().getPort()));
                    UDPSender.send(new Message(Message.MsgT.p, text, null, null, pair.getC2().getIP(), pair.getC2().getPort()));
                } else {
                    String text = "p,Number of elements " + pair.getNumOfElements() + " and " + numOfElements + "are not aligning.";
                    UDPSender.send(new Message(Message.MsgT.e, text, null, null, pair.getC1().getIP(), pair.getC1().getPort()));
                    UDPSender.send(new Message(Message.MsgT.e, text, null, null, pair.getC2().getIP(), pair.getC2().getPort()));
                }
            } else {
                createPair(client, id2, numOfElements);
            }
        }
    }

    private void createPair(Client client, int id2, int numOfElements) {
        Client c2 = new Client(new PlayerDO(id2, ""), "", 0); //dummy client to store id
        ClientPair pair = new ClientPair(client, c2, numOfElements);
        ServerState.getClientPairs().add(pair);
    }


}
