package logic.impl;

import database.domainObjects.GameDO;
import database.domainObjects.PlayerDO;
import logic.datastructures.Client;
import logic.datastructures.ClientPair;
import logic.datastructures.ServerState;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;

public class MultiplayerHandlerImpl extends MessageHandlerAbstract { //receives game-messages, creates and manages ongoing games
    @Override
    public void handleMessage(Message m) { //"id,pwd,pick"
        String[] content = m.getContent().split(",");
        if (!minContentLength(content, 3, Message.MsgT.m, m.getSourceIP(), m.getSourcePort())) return;
        PlayerDO player = new PlayerDO(parseInteger(content[0], Message.MsgT.m, m.getSourceIP(), m.getSourcePort()), content[1]);
        Client client = new Client(player, m.getSourceIP(), m.getSourcePort());
        int pick = parseInteger(content[2], Message.MsgT.m, m.getSourceIP(), m.getSourcePort());

        if (!validateCredentials(client, Message.MsgT.m)) return;


        ClientPair pair = getClientPair(player.getID());
        if (pair != null) { //players are connected
            if (!validatePick(pick, pair.getNumOfElements(), Message.MsgT.m, m.getSourceIP(), m.getSourcePort()))
                return;

            GameDO game = getGame(pair.getC1().getPlayer().getID(), pair.getC2().getPlayer().getID());

            if (game == null) { //one of the players starts a new game
                game = new GameDO(pair.getC1().getPlayer().getID(), pair.getC2().getPlayer().getID(), System.currentTimeMillis(), pick, null, pair.getNumOfElements());
                ServerState.getGames().add(game);

                //game started
                boolean isC1 = player.getID() == pair.getC1().getPlayer().getID();
                UDPSender.send(new Message(Message.MsgT.m, "s," + player.getID(), null, null, isC1 ? pair.getC2().getIP() : pair.getC1().getIP(), isC1 ? pair.getC2().getPort() : pair.getC1().getPort()));

            } else { //second player is taking their turn, therefore game already exists
                game.setC2pick(pick);
                pair.setTime(System.currentTimeMillis()); //time gets reset after each game so connection stays active

                //results get send to clients, game gets saved to db and removed from ServerState
                UDPSender.send(new Message(Message.MsgT.m, "r," + game.getC1pick(), null, null, pair.getC2().getIP(), pair.getC2().getPort()));
                UDPSender.send(new Message(Message.MsgT.m, "r," + game.getC2pick(), null, null, pair.getC1().getIP(), pair.getC1().getPort()));
                gameDao.insert(game);
                ServerState.getGames().remove(game);
            }

        } else {
            UDPSender.send(new Message(Message.MsgT.e, "m,No player connection.", null, null, m.getSourceIP(), m.getSourcePort()));
        }
    }

    private GameDO getGame(int id1, int id2) {
        for (GameDO g : ServerState.getGames()) {
            if (g.getID1() == id1 && g.getID2() == id2) {
                return g;
            }
        }
        return null;
    }
}
