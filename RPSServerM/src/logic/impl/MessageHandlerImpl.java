package logic.impl;

import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPReceiver;
import network.UDPSender;

public class MessageHandlerImpl extends MessageHandlerAbstract {
    private MessageHandler registrationHandler = new RegistrationHandlerImpl();
    private MessageHandler validationHandler = new ValidationHandlerImpl();
    private MessageHandler connectionHandler = new PairingHandlerImpl();
    private MessageHandler multiplayerHandler = new MultiplayerHandlerImpl();
    private MessageHandler singleplayerHandler = new SingleplayerHandlerImpl();
    private MessageHandler queryHandler = new QueryHandlerImpl();

    public MessageHandlerImpl() {
        UDPReceiver.setMessageHandler(this);
    }

    public void handleMessage(Message m) { //all incoming messages land here and get distributed to message-specific handlers
        if (m == null) return;

        switch (m.getType()) {
            case t: //confirms that server configuration on client side is correct
                UDPSender.send(new Message(Message.MsgT.t, "", null, null, m.getSourceIP(), m.getSourcePort()));
                break;
            case v:
                validationHandler.handleMessage(m);
                break;
            case r:
                registrationHandler.handleMessage(m);
                break;
            case p:
                connectionHandler.handleMessage(m);
                break;
            case m:
                multiplayerHandler.handleMessage(m);
                break;
            case s:
                singleplayerHandler.handleMessage(m);
                break;
            case q:
                queryHandler.handleMessage(m);
                break;
            default: //sends generic error
                UDPSender.send(new Message(Message.MsgT.e, "e,Invalid request.", null, null, m.getSourceIP(), m.getSourcePort()));
                break;
        }
    }

}
