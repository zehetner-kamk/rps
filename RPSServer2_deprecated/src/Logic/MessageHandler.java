package Logic;

import Database.DomainObjects.GameDO;
import Database.GameDao;
import Network.Message;
import Network.UDPSender;

import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

public class MessageHandler {
    private GameDao gameDao = new GameDao();
    private Random r = new Random();

    public void handleMessage(Message m) {
        String[] content = m.getContent().split(",");
        if (content.length < 2) { //message has to contain at least clientid and client pick
            UDPSender.send(new Message("Invalid message content.", null, null, m.getSourceIP(), m.getSourcePort()));
            return;
        }

        GameDO game = new GameDO(parseInteger(content[0], m.getSourceIP(), m.getSourcePort()), new Timestamp(System.currentTimeMillis()), parseInteger(content[1], m.getSourceIP(), m.getSourcePort()), r.nextInt(3));
        if (game.getID() == -2 || game.getCPick() == -2) return; //message is in invalid format

        int id = 0;
        if (game.getID() == -1) { //client connects for the first time
            List<Integer> ids = gameDao.getIDs();
            boolean validID = false;
            while (!validID) {
                id = r.nextInt(1000000);
                if (!ids.contains(id)) {
                    game.setID(id);
                    validID = true;
                }
            }
        }
        if(id!=0)
        if () {
            UDPSender.send(new Message("Invalid ID.", null, null, m.getSourceIP(), m.getSourcePort()));
            return;
        }

        if (content.length == 3) { //client defined custom number of elements, lets players play e.g. rps-lizard-spock
            int numOfElements = parseInteger(content[2], m.getSourceIP(), m.getSourcePort());
            if (numOfElements == -2) return;
            if (numOfElements > 2) {
                game.setNumOfElements(numOfElements);
                game.setSPick(r.nextInt(numOfElements));
            }
        }

        Message outM = new Message(game.getID() + "," + game.getSPick(), null, null, m.getSourceIP(), m.getSourcePort() + 1);

        gameDao.insert(game);
        UDPSender.send(outM);
    }

    protected int parseInteger(String intg, String ip, int port) {
        try {
            return Integer.parseInt(intg);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message("Integer " + intg + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }
}
