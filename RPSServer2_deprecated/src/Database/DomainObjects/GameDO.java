package Database.DomainObjects;

import java.sql.Timestamp;

public class GameDO {
    private int id;
    private Timestamp time;
    private int sPick;
    private int cPick;
    private int numOfElements;


    public GameDO(int id, Timestamp time, int cPick, int sPick) {
        this.id = id;
        this.time = time;
        this.cPick = cPick;
        this.sPick = sPick;
        this.numOfElements = 3;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public int getSPick() {
        return sPick;
    }

    public void setSPick(int sPick) {
        this.sPick = sPick;
    }

    public Integer getCPick() {
        return cPick;
    }

    public void setCPick(int cPick) {
        this.cPick = cPick;
    }

    public int getNumOfElements() {
        return numOfElements;
    }

    public void setNumOfElements(int numOfElements) {
        this.numOfElements = numOfElements;
    }
}
