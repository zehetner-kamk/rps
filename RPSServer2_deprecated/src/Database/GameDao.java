package Database;

import Database.DomainObjects.GameDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GameDao {
    private static final String TABLE_NAME = "game";

    public boolean insert(GameDO game) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (clientid, time, spick, cpick, numofelements) VALUES (?, ?, ?, ?,?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, game.getID());
                statement.setTimestamp(2, game.getTime());
                statement.setInt(3, game.getSPick());
                statement.setInt(4, game.getCPick());
                statement.setInt(5, game.getNumOfElements());

                if (statement.execute()) {
                    System.out.println("saved to db");
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<Integer> getIDs() {
        String selectStatement = "SELECT clientid FROM " + TABLE_NAME;

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(selectStatement)) {

                ResultSet result = statement.executeQuery();
                List<Integer> ids = new ArrayList<>();
                while (result.next()) {
                    ids.add(result.getInt(1));
                }
                return ids;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }


}
