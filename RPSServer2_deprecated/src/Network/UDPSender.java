package Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPSender {
    private static DatagramSocket socket;
    private static final int SEND_PORT = 30012;

    public static void send(Message m) {
        if (!realSend(m)) { //if sending fails, UDPSender tries to resend message up to 10 times with increasing intervals
            new Thread(() -> {
                Message copy = new Message(m);
                boolean success = false;
                for (int i = 0; i < 10; ++i) {
                    if (!success) {
                        try {
                            Thread.sleep(i * 1000);
                            success = UDPSender.realSend(copy);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }

    private static boolean realSend(Message m) {
        try {
            while (socket == null || socket.isClosed()) {
                socket = new DatagramSocket(SEND_PORT);
            }

            byte[] b = (m.getContent()).getBytes("UTF-8");
            DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(m.getDestinationIP()), m.getDestinationPort() + 1); //response gets sent to client sending port +1
            socket.send(packet);
            System.out.println("Sent " + new String(packet.getData(), "UTF-8"));

            return true;
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
