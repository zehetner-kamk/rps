package Network;

import Logic.MessageHandler;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPReceiver {
    private MessageHandler messageHandler = new MessageHandler();
    private DatagramSocket socket;
    private boolean stop;
    private static int SERVER_PORT = 30002;

    public void start() {
        stop = false;
        while (!stop) {
            try {
                while (socket == null || socket.isClosed()) {
                    socket = new DatagramSocket(SERVER_PORT);
                }

                DatagramPacket p = new DatagramPacket(new byte[64], 64);
                socket.receive(p);
                System.out.println("Received " + new String(p.getData(), "UTF-8"));

                Message m = new Message(new String(p.getData(), "UTF-8").replace("\0", ""), p.getAddress().toString().substring(1), p.getPort(), null, SERVER_PORT);
                messageHandler.handleMessage(m);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        this.stop = true;
    }
}