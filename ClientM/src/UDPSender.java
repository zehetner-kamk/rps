import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.Scanner;

public class UDPSender {
    private static DatagramSocket socket;
    private static final int SEND_PORT = 20002;


    public static void normal(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);
            while (true) {
                Scanner input = new Scanner(System.in);
                String message = input.nextLine();
                byte[] b = (message).getBytes("UTF-8");
                DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                socket.send(packet);
                System.out.println("SENT");
            }


        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }


    public static void attack(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);
            Random r = new Random();

            int i = 0;
            long before = System.currentTimeMillis();
            int numOfJoysticks = 25;
            while (System.currentTimeMillis() - before < 15000) {
                for (int j = 0; j < numOfJoysticks; ++j) {
                    String message = "d," + r.nextInt(numOfJoysticks) + ",0,0";
                    byte[] b = (message).getBytes("UTF-8");
                    DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                    socket.send(packet);
                    ++i;
                }
                if (i % 1000 == 0) System.out.println(i);


                try {
                    Thread.sleep(24); //~40 requests/sec/joystick
                } catch (InterruptedException e) {
                }
            }
            System.out.println("#J: " + numOfJoysticks + ", R/s: " + i / 15);


        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
