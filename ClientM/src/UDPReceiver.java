import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPReceiver {
    private static boolean stop;
    private static final int RECEIVE_PORT = 20003;

    public static void start() { //endless while loop that receives UDP packets and notifies messageHandlers
        DatagramSocket socket = null;
        stop = false;
        while (!stop) {
            try {
                while (socket == null || socket.isClosed()) {
                    socket = new DatagramSocket(RECEIVE_PORT);
                }

                DatagramPacket packet = new DatagramPacket(new byte[64], 64);
                socket.receive(packet);
                //System.out.println("RECEIVED " + new String(packet.getData(), "UTF-8").replace("\0",""));


            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }

            }
        }
    }

    public static void stop() {
        stop = true;
    }
}