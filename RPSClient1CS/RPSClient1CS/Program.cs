﻿/*
 * Title: Rock, Paper, Scissors
 * Author: Joona Tolonen
 */
using System;
using static System.Console;
using static System.Int32;

namespace RockPaperScissors {
    internal struct Player {
        internal string Name;
        internal bool IsHuman;
        internal int Choice;
        internal string[] ChoiceName;
    }

    internal static class Program {
        // Later on this should be replaced with an initialisation of the Server class
        private static readonly Random Rnd = new Random();
        private static readonly Server s = new Server();

        private static void Main() {
            WriteLine("Welcome to play Rock, Paper, Scissors!");

            var p1 = InitPlayer(true);
            var cmp = InitPlayer(false);

            Console.Write("How many rounds will be played? ");
            if (!TryParse(Console.ReadLine(), out var rounds)) {
                Console.WriteLine("Invalid rounds, will use 5.");
                rounds = 5;
            }

            Console.WriteLine("Excellent {0}! Let's play the best out of {1} rounds.", p1.Name, rounds);

            for (var i = 0; i < rounds; i++) {
                MakeChoice(ref p1);

                MakeChoice(ref cmp);

                WhoWon(p1, cmp);
            }
        }

        private static Player InitPlayer(bool isHuman) {
            var input = "";

            if (isHuman) {
                while (input != null && input.Length == 0) {
                    Write("What is your name? ");
                    input = ReadLine();
                }
            } else {
                input = "Computer";
            }

            return new Player {
                Name = input,
                IsHuman = isHuman,
                Choice = 0,
                ChoiceName = new[] { "Rock", "Paper", "Scissors" }
            };
        }

        private static void MakeChoice(ref Player p) {
            if (!p.IsHuman) {
                //p.Choice = Rnd.Next(3); //Current implementation
                //In future should be replaced with Server.GetNext()
                p.Choice = s.GetNext();
            } else {
                while (true) {
                    Write("Select (R)ock, (P)aper, or (S)cissors: ");

                    var userInput = ReadLine()?.Trim().ToLower();

                    if (userInput != null && userInput.StartsWith("r")) {
                        p.Choice = 0;
                        break;
                    }

                    if (userInput != null && userInput.StartsWith("p")) {
                        p.Choice = 1;
                        break;
                    }

                    if (userInput != null && userInput.StartsWith("s")) {
                        p.Choice = 2;
                        break;
                    }
                }
            }

            WriteLine($"{p.Name} selected: {p.ChoiceName[p.Choice]}");
        }

        private static void WhoWon(Player p1, Player p2) {
            if (p1.Choice == p2.Choice) {
                Console.WriteLine("Draw!");
            } else if ((p1.Choice == 0 && p2.Choice == 2) ||
                       (p1.Choice == 1 && p2.Choice == 0) ||
                       (p1.Choice == 2 && p2.Choice == 1)) {
                Console.WriteLine($"{p1.Name} won!");
            } else {
                Console.WriteLine($"{p2.Name} won!");
            }
        }
    }
}