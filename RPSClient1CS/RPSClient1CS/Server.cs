/*
 * Class that will take care of all communication with server.
 * Author: Clemens Zehetner
 */

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;


namespace RockPaperScissors {
    public class Server {
        private const string SERVER_ADDRESS = "86.50.253.157";
        private const int SERVER_PORT = 30000;

        private UdpClient sender = new UdpClient(20010);
        private IPEndPoint remotePt = new IPEndPoint(IPAddress.Parse(SERVER_ADDRESS), SERVER_PORT);

        private UdpClient receiver = new UdpClient(20011);
        private IPEndPoint localPt = new IPEndPoint(IPAddress.Any,  20011);



        public int GetNext() {
            sender.Send(new byte[1], 1, remotePt);
            byte[] bytes = receiver.Receive(ref localPt);
            Console.WriteLine("Received "+ Encoding.UTF8.GetString(bytes));

            return int.Parse(Encoding.UTF8.GetString(bytes));
        }
    }
}