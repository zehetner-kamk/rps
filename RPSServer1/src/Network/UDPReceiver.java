package Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Random;

public class UDPReceiver {
    private static DatagramSocket socket;
    private static boolean stop;
    private static final int SERVER_PORT = 30000;
    private static Random rand = new Random();

    public static void start() { //receives UDP message and answers with random number
        stop = false;
        while (!stop) {
            try {
                while (socket == null || socket.isClosed()) {
                    socket = new DatagramSocket(SERVER_PORT);
                }
                DatagramPacket p = new DatagramPacket(new byte[64], 64);
                socket.receive(p);
                System.out.println("Received from " + p.getAddress().toString().substring(1) + ":" + p.getPort());

                UDPSender.send(new Message(rand.nextInt(2) + "", p.getAddress().toString().substring(1), p.getPort()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void stop() {
        stop = true;
    }
}