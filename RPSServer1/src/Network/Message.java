package Network;

public class Message {
    private String content;
    private String destinationIP;
    private Integer destinationPort;

    public Message(String content, String destinationIP, Integer destinationPort) {
        this.content = content;
        this.destinationIP = destinationIP;
        this.destinationPort = destinationPort;
    }

    public Message(Message m) {
        this.content = m.getContent();
        this.destinationIP = m.getDestinationIP();
        this.destinationPort = Integer.valueOf(m.getDestinationPort());
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getDestinationIP() {
        return destinationIP;
    }

    public Integer getDestinationPort() {
        return destinationPort;
    }

}
