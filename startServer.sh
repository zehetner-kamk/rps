git pull

echo ""
echo "This script is going to kill all currently running Java and nodeJS processes."
echo "Press Enter to continue or Ctrl-C to cancel."
read x
sudo pkill "java"
echo "Killing old processes ..."
sleep 0.1 #wait until everything is killed

java -jar RPSServer${1}/out/artifacts/Server.jar ${2} &
