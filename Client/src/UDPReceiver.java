import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPReceiver {
    private static boolean stop;


    public static void start() {
        System.out.println("starting...");
        DatagramSocket socket=null;
        try {
            socket = new DatagramSocket(31001);
        }catch ( SocketException e){
            e.printStackTrace();
        }

        stop = false;
        while (!stop) {
            try {


                DatagramPacket p = new DatagramPacket(new byte[64], 64);
                socket.receive(p);

                System.out.println(new String(p.getData(), "UTF-8") + "\n" + p.getAddress().toString() + ":" + p.getPort());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public static void stop() {
        stop = true;
    }
}
